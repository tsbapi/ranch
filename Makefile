# See LICENSE for licensing information.

PROJECT = ranch

# Dependencies.

TEST_DEPS = ct_helper
dep_ct_helper = git git@git.ceb.loc/erlang/ct_helper.git 0.0.1-ceb

# Options.

COMPILE_FIRST = ranch_transport
PLT_APPS = crypto public_key ssl

# Standard targets.
RELX_URL = https://git.ceb.loc/erlang/relx/raw/v1.1.0-1/relx
PKG_FILE_URL ?= https://git.ceb.loc/erlang/erlang-mk/raw/master/packages.v2.tsv

include erlang.mk
